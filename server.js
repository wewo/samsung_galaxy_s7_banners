'use strict';

const path = require('path');
const express = require('express');
const app = express();

app.use(express.static(path.join(__dirname, '/views')));
app.use('/990x250', express.static('990x250'));

app.get('/', function (req, res) {
  res.render('index');
});

app.listen(3000, function () {
  console.log('Banner live on http://localhost:3000/');
});
