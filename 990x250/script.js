window.onload = function() {

    var cornerLogoSamsung = document.getElementById('corner-logo-samsung');
    var bigLogoSamsung = document.getElementById('big-logo-samsung');

    var phoneFront = document.getElementById('phone-front');
    var phoneTurn = document.getElementById('phone-turn');
    var phoneSide = document.getElementById('phone-side');
    var phoneBack = document.getElementById('phone-back');
    var phoneVr = document.getElementById('phone-vr');
    var twoPhones = document.getElementById('two-phones');
    var vrPhone_1 = document.getElementById('vr-phone-1');
    var vrPhone_2 = document.getElementById('vr-phone-2');

    var premiovyDizajnText = document.getElementById('premiovy-dizajn-text');
    var nabijanieText = document.getElementById('nabijanie-text');
    var vodaPrachText = document.getElementById('voda-prach-text');
    var fotoaparatText = document.getElementById('fotoaparat-text');
    var vrText = document.getElementById('vr-text');
    var zmeniPohladText = document.getElementById('zmeni-pohlad-text');
    var objednajteSiText = document.getElementById('objednajte-si-text');

    var tl = new TimelineLite();

    phoneFront.style.opacity = 1;

    function appear(element) {
        tl.to(element, 0.5, {
            'opacity': 1
        });
    }

    // phoneFront appear
    tl.to(phoneFront, 1, {
        'top': '10px',
        'left': '130px',
        'width': '185px',
        'height': '380px'
    });

    // premiovyDizajnText appear
    tl.to(premiovyDizajnText, 1, {
        'opacity': 1
    }, "-=0.5");

    /* --- */

    // phoneFront disappear
    tl.to(phoneFront, 0.5, {
        'opacity': 0,
        'delay': 1
    });

    // premiovyDizajnText disappear
    tl.to(premiovyDizajnText, 0.5, {
        'right': -550
    }, "-=0.5");

    /* --- */

    // phoneTurn appear
    appear(phoneTurn);

    // nabijanieText appear
    tl.to(nabijanieText, 0.5, {
        'right': '85px'
    }, "-=0.5");

    /* --- */

    // phoneTurn disappear
    tl.to(phoneTurn, 0.5, {
        'opacity': 0,
        'delay': 2
    });

    // nabijanieText disappear
    tl.to(nabijanieText, 0.5, {
        'right': -550
    }, "-=0.5");

    /* --- */

    // phoneSide appear
    appear(phoneSide);

    // vodaPrachText appear
    tl.to(vodaPrachText, 0.5, {
        'right': '45px'
    }, "-=0.5");

    /* --- */

    // phoneSide disappear
    tl.to(phoneSide, 0.5, {
        'opacity': 0,
        'delay': 2
    });

    // vodaPrachText disappear
    tl.to(vodaPrachText, 0.5, {
        'right': -650
    }, "-=0.5");

    /* --- */

    // phoneBack appear
    appear(phoneBack);

    // fotoaparatText appear
    tl.to(fotoaparatText, 0.5, {
        'right': '120px'
    }, "-=0.5");

    /* --- */

    // phoneBack disappear
    tl.to(phoneBack, 0.5, {
        'opacity': 0,
        'delay': 2
    });

    // fotoaparatText disappear
    tl.to(fotoaparatText, 0.5, {
        'right': -650
    }, "-=0.5");

    /* --- */

    // phoneVr appear
    appear(phoneVr);

    // vrText appear
    tl.to(vrText, 0.5, {
        'right': '55px'
    }, "-=0.5");

    /* --- */

    // cornerLogoSamsung disappear
    tl.to(cornerLogoSamsung, 0.5, {
        'opacity': 0,
        'delay': 2
    });

    // phoneVr disappear
    tl.to(phoneVr, 0.5, {
        'opacity': 0
    }, "-=0.5");

    // vrText disappear
    tl.to(vrText, 0.5, {
        'opacity': 0
    }, "-=0.5");

    /* --- */

    // twoPhones appear
    tl.to(twoPhones, 0.5, {
        'opacity': 1
    });

    // zmeniPohladText appear
    tl.to(zmeniPohladText, 0.5, {
        'left': '38px'
    });

    // bigLogoSamsung appear
    tl.to(bigLogoSamsung, 0.5, {
        'right': '60px'
    }, "-=0.5");

    /* --- */

    // twoPhones disappear
    tl.to(twoPhones, 0.5, {
        'opacity': 0,
        'delay': 2
    });

    // zmeniPohladText disappear
    tl.to(zmeniPohladText, 0.5, {
        'opacity': 0
    }, "-=0.5");

    // bigLogoSamsung disappear
    tl.to(bigLogoSamsung, 0.5, {
        'opacity': 0
    }, "-=0.5");

    // cornerLogoSamsung appear
    tl.to(cornerLogoSamsung, 0.5, {
        'opacity': 1
    }, "-=0.5");

    /* --- */

    // vrPhone1 appear
    tl.to(vrPhone_1, 0.5, {
        'top': '60px'
    });

    // vrPhone2 appear
    tl.to(vrPhone_2, 0.5, {
        'top': '52px'
    }, "-=0.5");

    // objednajteSiText appear
    tl.to(objednajteSiText, 0.5, {
        'right': '55px'
    }, "-=0.5");

    /* --- */

    tl.pause();
    tl.resume();
    tl.timeScale(1);
};
